const name = 'Solid';
const style = { 'background-color': '#2c4f7c', color: '#FFF' };

export function HelloWorld() {
  return <div style={style}>Hello {name}!</div>;
}
